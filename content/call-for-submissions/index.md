+++
title = "Call for submissions"
email_blurb = """
Thanks for reading and for continuing to make this project what it is.

For the sake of those of you playing along at home this call for submissions is 123 words
"""
+++


Hi there! Thanks for coming. I’m going to try to be brief.

The next issue of TINWIB* will be called

In Short: Belong!

And will be somewhat of an experimental issue.

We’re asking that all poems submitted should be no longer than 20 lines long

And that all prose submitted should not exceed 500 words.

The deadline will be May 7th 2021 and the issue will be released on May 31st 2021,
and distributed mainly by mail, in line with the new normal.