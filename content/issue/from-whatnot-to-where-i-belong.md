---
date: 2022-07-04T13:08:33.300Z
title: From Whatnot To Where I Belong
issue: "3"
editor:
  - Saul Philbin Bowman
layout:
  - Beibhinn Delaney
printer:
  - Aungier Print Ltd.
binder:
  - Aungier Print Ltd.
circulation: 300
size: A5
pages: 41 pp. + cover
inside: white 100gsm
cover: white 100gsm
binding: saddle stitched
contributors:
  - Saul Philbin Bowman
  - Richard Howard
  - Gareth Stack
  - Caroline Spencer
  - Stephen Clare
  - Cal Folger Day
  - Melissa Ridge
  - Bernard O’Rourke
  - Beibhinn Delaney
  - Dave O’Hara
  - Rebecca Kennedy
  - Yasmine Colijn
  - Colin
  - Jake Regan
venue: BelloBar Portobello Dublin 8
acts:
  - DANI
  - Andy Train
  - Matt Creedon
  - The Journals
  - BIRDS
  - The Late David Turpin
---
