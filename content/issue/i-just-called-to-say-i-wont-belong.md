---
date: 2022-07-04T13:12:38.200Z
title: I Just Called to Say I Won't Belong
issue: "4"
editor:
  - Saul Philbin Bowman
layout:
  - The Gross Anatomy
printer:
  - Aungier Print Ltd.
binder:
  - Aungier Print Ltd.
circulation: 400
size: A5
pages: 42pp. + cover
inside: pink 80gsm
cover: pink 80gsm
binding: saddle stitched
contributors:
  - Alain Freudiger
  - Rosi Leopard
  - Hugh Hick
  - Kevin Barrington
  - John Cummins
  - Alvy Carragher
  - Richard Howard
  - Stephen Clare
  - Alicia Byrne Keane
  - John Fitzsimmons
  - Dane Scott
  - Sarah Zed
  - James Feeney
  - Ian McCafferty
  - Ozgecan
  - Tim Fernley
  - Yasmine Colijn
  - Jessica Bernard
  - Hollie Leddy Flood
  - Ruth
  - Saul Philbin Bowman
  - Paul Curran
  - Stephen Totterdell
  - Mike Timms
  - Oisín McCole
  - Andy Apples
venue: The Workman’s Club 10 Wellington Quay Dublin 2
acts:
  - Naoise Roo
  - DANI
  - Oisín McCole
  - Participant
  - Mongoose
  - Segrasso
  - Turning Down Sex
---
