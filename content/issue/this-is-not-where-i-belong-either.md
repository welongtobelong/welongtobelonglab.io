---
date: 2022-07-04T00:53:32.000Z
title: This Is Not Where I Belong Either
issue: 2
editor:
  - Saul Philbin Bowman
layout:
  - Beibhinn Delaney
printer:
  - Beibhinn Delaney
binder:
  - Beibhinn Delaney
  - Eimear Kilgarriff
  - Darragh McCabe
circulation: 200
size: A5
pages: 48 pp. + cover
inside: white 80gsm
cover: tracing 80gsm, white 80gsm
binding: saddle stitched
contributors:
  - Stephen Totterdell
  - Beibhinn Delaney
  - Ian Maleney
  - Sarah Anne Prosser
  - Jessica McWhirt
  - Colm Kearns
  - Gareth Stack
  - Saul Philbin Bowman
  - Richard Howard
  - Tim Fernley
venue: The Mercantile Venue Dame St. Dublin 2
acts:
  - DANI
  - Segrasso
  - Richard Richard
  - Saint Yorda
  - Mylets
  - Turning Down Sex
---
