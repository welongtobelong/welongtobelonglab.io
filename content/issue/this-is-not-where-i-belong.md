---
size: A5
pages: 14 pp. + cover
binding: glue
acts:
  - Séamus Hanly
  - Tara Masterson Hally
  - Rhino Magic
  - Ginola
date: 2022-07-04T00:42:31.800Z
title: This Is Not Where I Belong*
editor:
  - Saul Philbin Bowman
layout:
  - Beibhinn Delaney
binder:
  - Beibhinn Delaney
folder: null
circulation: 100
cover: brown 120gsm
venue: The Mercantile Venue Dame St. Dublin 2
contributors:
  - Beibhinn Delaney
  - Saul Philbin Bowman
  - Kilian McMahon
  - Richard Howard
  - Colm Kearns
  - Patrick Kindlon
  - Ana Novacic
  - Tim Fernley
issue: 1
printer:
  - Beibhinn Delaney
inside: white 80gsm
---
